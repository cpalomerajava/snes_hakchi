<!--- 2019-04-28T12:19:00.0000000-05:00 -->
**Reminder:** If you're modding a sega mini, the included USB cable will not work.

If you have any problems using hakchi2 ce, we recommend you join the discord server where all of the hakchi and hakchi2 ce developers are present.

[https://discord.gg/C9EDFyg](https://discord.gg/C9EDFyg)  

You can also ask any questions on reddit, but the responses may be delayed.

[https://reddit.com/r/minisnesmods/](https://reddit.com/r/minisnesmods/)

**Have a nice day**

*Team Shinkansen*
